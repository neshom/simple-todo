export default {
  title: "عنوان",
  minCharaters3: "حداقل تعداد حروف ۳ عدد می باشد",
  maxCharaters50: "حداکثر تعداد حروف 50 عدد می باشد",
  thisFieldIsRequired: "تکمیل این بخش الزامی است",
  add: "ثبت",
  delete: "حذف کردن",
  remove: "حذف تکمیل شده ها",
  edit: "ویرایش",
  cancel: "لغو کردن",
  save: "ذخیره کردن",
  editTodo: "ویرایش کار",
  todo: "مدیریت کارها",
};
