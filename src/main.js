import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import VueI18n from "vue-i18n";
import messages from "./i18n/";
import UUID from "vue-uuid";

Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: "fa", // set locale
  messages, // set locale messages
});
// const UniqueId = require("vue-unique-id");

Vue.config.productionTip = false;
new Vue({
  vuetify,
  render: (h) => h(App),
  i18n,
  UUID,
}).$mount("#app");
